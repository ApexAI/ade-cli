# Copyright 2017 - 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Communicate with docker registry"""

import asyncio
from subprocess import CalledProcessError, DEVNULL

import aiohttp
from .credentials import get_credentials
from .utils import run
from .utils import run_shell_cmd


class NoSuchImage(Exception):
    """Requested image is not available"""


class NoSuchTag(Exception):
    """Requested tag does not exist for image.

    raise NoSuchTag(img, tag, available_tags)
    """


class TokenError(Exception):
    """Error obtaining token for URL. Auth data is wrong"""


class Image:
    """Data model for an image in a docker registry."""
    registry = None
    namespace = None
    name = None
    tag = 'latest'

    @property
    def fqn(self):
        """Fully qualified name of a docker image including the tag"""
        return f'{self.fullname}:{self.tag}'  # noqa: E231

    @property
    def fullname(self):
        """Fully qualified name without the tag"""
        if self.registry and self.namespace:
            return f'{self.registry}/{self.namespace}/{self.name}'
        if self.registry and not self.namespace:
            return f'{self.registry}/{self.name}'
        if self.namespace:
            return f'{self.namespace}/{self.name}'
        return f'{self.name}'

    @property
    def api_url(self):
        """URL for API of the image's registry"""
        if self.registry:
            return f'https://{self.registry}/v2'  # noqa: E231
        return None

    @property
    def tags_url(self):
        """URL for API endpoint for list of tags for image"""
        if self.registry:
            return (f'https://{self.registry}/v2/{self.namespace}/'  # noqa: E231
                    f'{self.name}/tags/list')
        return None

    def __init__(self, fqn):
        components = fqn.split('/')
        if ':' in components[-1]:
            self.name, self.tag = components.pop().split(':')
        else:
            self.name = components.pop()

        if components and '.' in components[0]:
            self.registry = components.pop(0)

        if components:
            self.namespace = '/'.join(components)

    def __repr__(self):
        return f"Image('{self.fqn}')"


async def adjust_tag(client, image, selectors):
    """Adjust image tag according to selectors and availability"""
    tags = await get_tags(client, image)
    for name, tag in selectors:
        if image.name == name:
            if tag not in tags:
                raise NoSuchTag(image, tag, sorted(tags))
            image.tag = tag
            break
        if name is None and tag in tags:
            image.tag = tag
            break


async def adjust_tags(images, selectors):
    """Adjust tags of images according to selectors and availability"""
    async with aiohttp.ClientSession() as client:
        reqs = [adjust_tag(client, img, selectors) for img in images]
        await asyncio.gather(*reqs, return_exceptions=True)


async def get(client, url, registry, accept=None, stream=False):
    """Get url, implicitly handling authentication"""
    headers = {}
    accept = accept or 'application/json'
    if accept:
        headers['Accept'] = accept

    resp = await client.get(url, headers=headers)
    if resp.status == 401:
        token = await _authtoken(client, resp.headers, registry)
        headers['Authorization'] = f'Bearer {token}'
        resp = await client.get(url, headers=headers)

    if stream:
        return resp.content
    return await resp.json(content_type=accept)


def login(img):
    """Login into registry serving image"""
    username, token = get_credentials(img.registry).values()
    cp = run(['docker', 'login', '--username', username, '--password-stdin', img.registry],
             input=token.encode('utf-8'), check=False)
    if cp.returncode != 0:
        username, token = get_credentials(img.registry, reset=True).values()
        cp = run(['docker', 'login', '--username', username, '--password-stdin', img.registry],
                 input=token.encode('utf-8'), check='exit')


def pull_if_missing(image):
    """Pull an image if it does not exists locally"""
    try:
        run(['docker', 'image', 'inspect', '--format', '{{.Id}}', image.fqn],
            stdout=DEVNULL, stderr=DEVNULL)
    except CalledProcessError:
        pass
    else:
        return

    try:
        run(['docker', 'pull', image.fqn])
    except CalledProcessError:
        login(image)
        run(['docker', 'pull', image.fqn], check='exit')


async def get_image_id(image_name: str) -> bytes:
    """Get the ID of an image and print an error if the docker command fails"""
    proc = await run_shell_cmd(f'docker image inspect --format {{{{.Id}}}} {image_name}',
                               stdout=asyncio.subprocess.PIPE,
                               stderr=asyncio.subprocess.PIPE)
    proc.report_failure()
    return proc.stdout


async def pull_image(img: Image) -> None:
    """Pulls the docker image.

    If it fails, log in the registry and try again. If the second trial fails, exits.
    """
    cmd = f'docker pull {img.fqn}'
    pull_proc = await run_shell_cmd(cmd)
    if pull_proc.return_code != 0:
        login(img)
        pull_proc = await run_shell_cmd(cmd)
        pull_proc.report_failure(exit_on_error=True)


async def update_image(img):
    """Update an image from docker registry"""
    old_image_id = await get_image_id(img.fqn)
    await pull_image(img)
    new_image_id = await get_image_id(img.fqn)
    return new_image_id != old_image_id


async def get_tags(client, image):
    """Get list of tags for image from registry"""
    data = await get(client, image.tags_url, registry=image.registry)
    try:
        return [x for x in data['tags'] if not x.startswith('commit-')]
    except KeyError as e:
        raise NoSuchImage(image) from e


async def _authtoken(client, headers, registry):
    info = {
        k: v.split('"')[1] for k, v in [
            x.split('=') for x in headers['Www-Authenticate'].split()[1].split(',')
        ]
    }
    info['client_id'] = 'ade'
    auth = aiohttp.BasicAuth(*get_credentials(registry).values())
    resp = await client.get(info.pop('realm'), auth=auth, params=info)
    resp = await resp.json()
    try:
        token = resp['token']
    except KeyError as e:
        raise TokenError() from e
    return token
