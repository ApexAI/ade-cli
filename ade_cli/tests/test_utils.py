# SPDX-License-Identifier: Apache-2.0

import asyncio
import pytest

from ade_cli.utils import Process, run_shell_cmd


def test_report_no_failure(capsys):
    proc = Process('my_cmd', 'Everything ok', None, 0)
    proc.report_failure()
    captured = capsys.readouterr()
    assert captured.out == ''
    assert captured.err == ''


def test_report_failure_without_stderr(capsys):
    proc = Process('my_cmd', None, None, -1)
    proc.report_failure()
    captured = capsys.readouterr()
    assert captured.out == 'The following command returned a nonzero exit code (-1):\nmy_cmd\n'
    assert captured.err == ''


def test_report_failure_with_stderr(capsys):
    proc = Process('my_cmd', None, bytes('Something wrong happened', 'utf-8'), -1)
    proc.report_failure()
    captured = capsys.readouterr()
    assert captured.out == "The following command returned a nonzero exit code (-1):\nmy_cmd\n" \
                           "Something wrong happened \n\n"
    assert captured.err == ''


def test_report_failure_with_exit():
    proc = Process('my_cmd', None, None, -42)
    with pytest.raises(SystemExit) as e:
        proc.report_failure(exit_on_error=True)
    assert e.type == SystemExit
    assert e.value.code == -42


def test_valid_shell_command_no_pipe():
    proc = asyncio.run(run_shell_cmd('echo "hello"'))
    assert proc.cmd == 'echo "hello"'
    assert proc.stdout is None
    assert proc.stderr is None
    assert proc.return_code == 0


def test_valid_shell_command_pipe():
    proc = asyncio.run(run_shell_cmd('echo "hello"',
                                     stdout=asyncio.subprocess.PIPE,
                                     stderr=asyncio.subprocess.PIPE))
    assert proc.stdout == b'hello\n'


def test_invalid_shell_command_pipe():
    proc = asyncio.run(run_shell_cmd('ls /zzz',
                                     stdout=asyncio.subprocess.PIPE,
                                     stderr=asyncio.subprocess.PIPE))
    assert proc.stderr == b"ls: cannot access '/zzz': No such file or directory\n"
